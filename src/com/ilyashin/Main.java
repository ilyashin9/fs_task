package com.ilyashin;

public class Main {

    public static void main(String[] args) {
        SortDoubles sortDoubles = new SortDoubles();
        Manager manager = new Manager(sortDoubles);
        try {
            //check amount of arguments
            if (args.length == 2) {
                manager.start(args[0], args[1]);
            }
            else
                System.out.println("Wrong number of arguments");
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }


    }
}
