package com.ilyashin;

import java.io.*;
import java.util.*;

public class Manager {
    private Triangle triangle;
    private Comparator<Double> sortDoubles;

    /**
     * Constructor
     * @param comparator Concrete Comparator
     */
    public Manager(Comparator<Double> comparator) {
        sortDoubles = comparator;
        triangle = null;
    }

    /**
     * Method that manage program execution
     * @param inPath path to input file
     * @param outPath path to output file
     * @throws Exception rethrows exception from another methods
     */
    public void start(String inPath, String outPath) throws Exception{
        checkFiles(inPath, outPath);
        readFile(inPath);
        writeInFile(outPath);
    }

    /**
     * Checker for files(find exists, file is not empty etc)
     * @param inPath path to input file
     * @param outPath path to output file
     * @throws Exception if file is incorrect
     */
    private void checkFiles(String inPath, String outPath) throws Exception{
        File in = new File(inPath);
        if (!in.exists())
            throw new Exception("Can not find file " + inPath);
        File out = new File(outPath);
        if (!out.exists())
            throw new Exception("Can not find file " + outPath);

        if (!in.canRead())
            throw new Exception("Can not read file " + inPath);
        if (!out.canWrite())
            throw new Exception("Can not write to file " + outPath);
        if (in.length() == 0) {
            throw new Exception("File " + inPath + " is empty");
        }
    }

    /**
     * reads data from file
     * @param path path to input file
     */
    private void readFile(String path) {
        File f = new File(path);
        FileInputStream fileInputStream;
        Scanner fileScanner;

        try {
            String line;
            fileInputStream = new FileInputStream(f);
            fileScanner = new Scanner(fileInputStream, "UTF-8");
            int count = 0;

            while (fileScanner.hasNextLine()) {
                count++;
                line = fileScanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                List<Long> list = new ArrayList<>();
                boolean flag = false;

                while (lineScanner.hasNextLong()) {
                    list.add(lineScanner.nextLong());
                    if (!checkData(list) && !lineScanner.hasNextLong() && lineScanner.hasNext()) {
                        showError(1, count);
                        flag = true;
                    }
                }

                if (checkData(list)) {
                    Vertex vertex1 = new Vertex(list.get(0), list.get(1));
                    Vertex vertex2 = new Vertex(list.get(2), list.get(3));
                    Vertex vertex3 = new Vertex(list.get(4), list.get(5));
                    findMax(vertex1, vertex2, vertex3);
                }
                else if (!flag)
                    showError(2, count);

            }

            fileScanner.close();
            fileInputStream.close();
            fileScanner.close();

        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    /**
     * Shows info for wrong input
     * @param code error code
     * @param lineNumber line where error was found
     */
    private void showError(int code, int lineNumber) {
        switch (code) {
            case (1):
                System.out.println("In line " + lineNumber + " wrong dataType");
                break;
            case (2):
                System.out.println("In line " + lineNumber + " wrong amount of coordinates");
                break;
        }
    }

    /**
     * writes in file
     * @param path path to output file
     * @throws IOException if we have some problems with file
     */
    private void writeInFile(String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));

        if (triangle == null) {
            writer.close();
            return;
        }
        writer.write(triangle.toString());
        writer.close();
    }

    /**
     * Compare existing triangle with new triangle
     * @param vertex1 first vertex of new triangle
     * @param vertex2 second vertex of new triangle
     * @param vertex3 third vertex of new triangle
     */
    private void findMax(Vertex vertex1, Vertex vertex2, Vertex vertex3) {
        if (isCorrectTriangle(vertex1, vertex2, vertex3)) {
            Triangle tempTriangle = new Triangle(vertex1, vertex2, vertex3);

            if (compareTriangles(tempTriangle) > 0)
                triangle = tempTriangle;
        }
    }

    /**
     * Checks amount of read coordinates
     * @param list list with coordinates
     * @return true if amount is correct, otherwise - false
     */
    private boolean checkData(List<Long> list) {
        return list.size() == 6;
    }

    /**
     * Compare triangles areas using comporator
     * @param tempTriangle new triangle
     * @return 1 if new triangle's area is bigger, -1 if it's smaller, 0 if they are equals
     */
    private int compareTriangles(Triangle tempTriangle) {
        if (triangle == null)
            return 1;
        return sortDoubles.compare(tempTriangle.getArea(), triangle.getArea());
    }

    /**
     * check that given vertexes forms isosceles triangle
     * @param vertex1 triangle's first vertex
     * @param vertex2 triangle's second vertex
     * @param vertex3 triangle's third vertex
     * @return true if triangle is correct, otherwise - false
     */
    private boolean isCorrectTriangle(Vertex vertex1, Vertex vertex2, Vertex vertex3) {
        Double firstSide = calculateSide(vertex1.getPointX(), vertex1.getPointY(), vertex2.getPointX(), vertex2.getPointY());
        Double secondSide = calculateSide(vertex1.getPointX(), vertex1.getPointY(), vertex3.getPointX(), vertex3.getPointY());
        Double thirdSide = calculateSide(vertex2.getPointX(), vertex2.getPointY(), vertex3.getPointX(), vertex3.getPointY());

        return isTriangle(firstSide, secondSide, thirdSide) && isIsoscelesTriangle(firstSide, secondSide, thirdSide);
    }

    /**
     * check that triangle is isosceles
     * @param firstSide triangle's first side
     * @param secondSide triangle's second side
     * @param thirdSide triangle's third side
     * @return true if triangle is isosceles, otherwise - false
     */
    private boolean isIsoscelesTriangle(Double firstSide, Double secondSide, Double thirdSide) {
        if (sortDoubles.compare(firstSide, secondSide) == 0)
            return true;

        if (sortDoubles.compare(secondSide, thirdSide) == 0)
            return true;

        return sortDoubles.compare(firstSide, thirdSide) == 0;
    }

    /**
     * Check that sum of triangle's two sides always bigger than third side
     * @param firstSide triangle's first side
     * @param secondSide triangle's second side
     * @param thirdSide triangle's third side
     * @return true if triangle is correct, otherwise - false
     */
    private boolean isTriangle(Double firstSide, Double secondSide, Double thirdSide) {
        Double sum = firstSide + secondSide;

        if (sortDoubles.compare(sum, thirdSide) > 0) {
            sum = secondSide + thirdSide;
            if (sortDoubles.compare(sum, firstSide) > 0) {
                sum = firstSide + secondSide;
                return sortDoubles.compare(sum, thirdSide) > 0;
            }
        }
        return false;
    }

    /**
     * Calculates triangle's side by given coordinates
     * @param x1 first vertex x coordinate
     * @param y1 first vertex y coordinate
     * @param x2 second vertex x coordinate
     * @param y2 second vertex y coordinate
     * @return length of triangle's side
     */
    private Double calculateSide(Long x1, Long y1, Long x2, Long y2) {
        Long xDifference = Math.abs(x1 - x2);
        Long yDifference = Math.abs(y1 - y2);

        return Math.sqrt(xDifference*xDifference + yDifference*yDifference);
    }
}
