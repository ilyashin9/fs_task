package com.ilyashin;

public class Triangle {
    private Vertex firstVertex;
    private Vertex secondVertex;
    private Vertex thirdVertex;
    private double area;

    /**
     * Constructor
     * @param firstVertex Triangle's first vertex
     * @param secondVertex Triangle's second vertex
     * @param thirdVertex Triangle's third vertex
     */
    public Triangle(Vertex firstVertex, Vertex secondVertex, Vertex thirdVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.thirdVertex = thirdVertex;
        calculateArea();
    }

    /**
     * Calculate triangle's area by given coordinates of vertexes
     */
    private void calculateArea() {
        area = Math.abs((secondVertex.getPointX() - firstVertex.getPointX())*(thirdVertex.getPointY() - firstVertex.getPointY()) -
                (thirdVertex.getPointX() - firstVertex.getPointX())*(secondVertex.getPointY() - firstVertex.getPointY()));
        area /= 2;
    }

    /**
     * getter for first Vertex
     * @return Vertex class object
     */
    public Vertex getFirstVertex() {
        return firstVertex;
    }

    /**
     * getter for second Vertex
     * @return Vertex class object
     */
    public Vertex getSecondVertex() {
        return secondVertex;
    }

    /**
     * getter for third Vertex
     * @return Vertex class object
     */
    public Vertex getThirdVertex() {
        return thirdVertex;
    }

    /**
     * getter for Triangle's area
     * @return calculated triangle area
     */
    public double getArea() {
        return area;
    }

    /**
     * Conversion to string
     * @return String representation
     */
    @Override
    public String toString() {
        return firstVertex + " " + secondVertex + " " + thirdVertex;
    }

    /**
     * determine if objects are equal
     * @param obj another object of this class
     * @return true - if objects equals, else - false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        Triangle that = (Triangle)obj;

        return firstVertex.equals(that.getFirstVertex()) &&
                secondVertex.equals(that.getSecondVertex()) &&
                thirdVertex.equals(that.getThirdVertex()) &&
                area == that.getArea();
    }
}
