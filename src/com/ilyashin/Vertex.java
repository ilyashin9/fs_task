package com.ilyashin;


public class Vertex {
    private long pointX;
    private long pointY;

    /**
     * Constructor
     * @param pointX x coordinate of vertex
     * @param pointY y coordinate of vertex
     */
    public Vertex(long pointX, long pointY) {
        this.pointX = pointX;
        this.pointY = pointY;
    }

    /**
     * getter for pointX field
     * @return x coordinate of vertex
     */
    public long getPointX() {
        return pointX;
    }

    /**
     * getter for pointY field
     * @return y coordinate of vertex
     */
    public long getPointY() {
        return pointY;
    }

    /**
     * Conversion to string
     * @return String representation
     */
    @Override
    public String toString() {
        return pointX + " " + pointY;
    }

    /**
     * determine if objects are equal
     * @param obj another object of this class
     * @return true - if objects equals, else - false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        Vertex that = (Vertex)obj;

        return pointX == that.getPointX() && pointY == that.getPointY();
    }
}
