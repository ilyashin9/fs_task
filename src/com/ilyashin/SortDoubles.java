package com.ilyashin;

import java.util.Comparator;

public class SortDoubles implements Comparator<Double> {
    /**
     * Comporator for doubles with accuracy 10e-5
     * @param a first object
     * @param b second object
     * @return 1 - if a > b, 0 - if a == b, -1 - if a < b
     */
    public int compare(Double a, Double b) {
        double accuracy = 0.00001;
        double res = Math.abs(a - b);
        if (res <= accuracy) {
            return 0;
        }

        if ((a - b) > accuracy) {
            return 1;
        }
        return -1;
    }
}
